


import 'package:doe_registration_app/common/theme_helper.dart';
import 'package:flutter/cupertino.dart';

import 'package:flutter/material.dart';

import 'widgets/header_widget.dart';
import 'widgets/profile_page.dart';

 class LoginPage extends StatefulWidget{
  const LoginPage({Key? key}): super(key:key);

  @override
    _LoginPageState createState() => _LoginPageState();
 }

class _LoginPageState extends State<LoginPage>{
  double _headerHeight = 250;
  Key _formKey =GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              height: _headerHeight,
              child: HeaderWidget(_headerHeight, true, Icons.login_rounded),
            ),
            SafeArea(
              child: Container(
                  child: Column(
                    children: [
                      Text(
                        'WELCOME',
                        style: TextStyle(fontSize: 40, fontWeight: FontWeight.bold),
                      ),
                      SizedBox(height: 30.0),
                      Form(
                        key: _formKey,
                        child: Column(
                          children: [
                            TextField(
                              decoration: ThemeHelper().textInputDecoration('User Name', 'Enter Your Name'),
                            ),
                            SizedBox(height: 30.0),
                            TextField(
                              obscureText: true,
                              decoration: ThemeHelper().textInputDecoration('Password', 'Enter Your Password'),
                            ),
                            SizedBox(height: 16.0),
                            Container(
                              child: Text('Forgot Password'),
                            ),
                            SizedBox(height: 16.0),
                            Container(
                              decoration: ThemeHelper().buttonBoxDecoration(context),
                              child: ElevatedButton(
                                style: ThemeHelper().buttonStyle(),
                                child: Padding(
                                  padding: EdgeInsets.fromLTRB(40, 10, 40, 10),
                                  child: Text('Sign In'.toUpperCase(), style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white)),
                                ),
                                onPressed: (){
                                  Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => ProfilePage()));
                                },
                              ),

                            ),
                            SizedBox(height: 16.0),
                            Container(
                              child: Text('Don\'t have an account? Create '),
                            ),
                          ],
                        ),
                      ),
                    ],
                  )
              ),
            ),
          ],
        ),
      ),
    );
  }

}





